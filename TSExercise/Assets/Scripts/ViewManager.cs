﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewManager : MonoBehaviour 
{
	System.DateTime currentTime;
	int second;

	List<Transform> spawnedCubes = new List<Transform>();
	Vector3 spawnPos = Vector3.zero;
	int cubeIndex;
	bool canSpawn = true;

	[Header("UI Elements")]
	public Text timerText;

	[Header("View Objects")]
	public GameObject viewOneObj;
	public GameObject viewTwoObj;

	void Update ()
	{
		//save current time variable
		currentTime = System.DateTime.UtcNow;

		//write current time to ui element
		timerText.text = currentTime.ToString();

		//save the last second
		second = currentTime.Second;

		//check if second is even or odd and spawn accordingly
		if (second % 2 == 0 && canSpawn) 
		{
			//only spawn when we arent spawning another cube
			canSpawn = false;
			StartCoroutine (SpawnCube ());
		}
	}

	IEnumerator SpawnCube ()
	{
		//if we dont have 10 cubes, lets create them
		if (spawnedCubes.Count < 10)
		{
			//create new cube
			GameObject cube = GameObject.CreatePrimitive (PrimitiveType.Cube);
			//position new cube
			cube.transform.position = spawnPos;
			//space out the next cube
			spawnPos.x += 2;
			//add the new cube to our list of created cubes
			spawnedCubes.Add (cube.transform);
			//count up the cube index
			cubeIndex++;
		} 
		else
		{
			//if we have 10 cubes, simply iterate through at the same rate and change their color

			//if the last cube we manipulated was the last one, reset back to the first
			if (cubeIndex == 10)
				cubeIndex = 0;

			//grab the cube at the current index and change its color
			spawnedCubes [cubeIndex].gameObject.GetComponent<MeshRenderer> ().material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
			//count up the cube index
			cubeIndex++;
		}

		//wait for the odd second
		yield return new WaitForSeconds (1);
		//re-enable cube spawning
		canSpawn = true;
	}

	public void ToggleView ()
	{
		//toggle between views and activate associated objects
		if (viewOneObj.activeSelf) 
		{
			viewOneObj.SetActive (false);
			viewTwoObj.SetActive (true);
		} 
		else 
		{
			viewOneObj.SetActive (true);
			viewTwoObj.SetActive (false);
		}
	}
}

